const
  net = require('net'),
  cs = require('color-stdout'),
  keypress = require('keypress'),
  readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

let nickname, client;

// Call function for create user nickname
addUser(connectToChat);

// Function for create socket and connecting to chat
function connectToChat() {
  process.stdout.write('\033c');

  let tempMsg = '';

  keypress(process.stdin);
  process.stdin.on('keypress', function(ch, key){
    if(key && key.name === 'backspace'){
      tempMsg = tempMsg.slice(0, -1);
    } else
      tempMsg += ch;
  });
  process.stdin.setRawMode(true);
  process.stdin.resume();

  client = new net.Socket();
  client.connect(3000, '127.0.0.1', () => {
    cs.green(`Success connected. Your chat name is ${nickname}\n`);

    // Send nickname for first connection
    client.write(JSON.stringify({'cmd': 'setName', 'content': nickname}));
  });

  // Handle on socket data event
  client.on('data', data => {

    // Parse data
    let message = JSON.parse(data);

    if(message.content !== 'user exist'){
      // Print own message
      if(message.nickname == nickname){
        cs.white(`${new Date(message.timestamp).toLocaleTimeString()}: `);
        cs.magenta(`you > ${message.content}\n`);
        process.stdout.write('\033[K');
        cs.white('### Your message:\n');
      }
      // Print other user message
      else{
        process.stdout.write('\033[1A');
        process.stdout.write('\033[1K');
        process.stdout.cursorTo(0);
        cs.white(`${new Date(message.timestamp).toLocaleTimeString()}: `);
        cs.yellow(`${message.nickname} > ${message.content}\n`);
        process.stdout.write('\033[K');
        cs.white('### Your message:\n');
        cs.green(tempMsg);
      }
      // If nickname exist print error and asks to input new nickname
    } else {
      process.stdout.write('\033[1A');
      process.stdout.write('\033[2K');
      process.stdout.cursorTo(0);
      cs.red(`User ${message.nickname} is exists\n`);
      addUser(function(){client.write(JSON.stringify({'cmd': 'setName', 'content': nickname}));});
    }
  });

  readline.on('line', input => {

    // Check empty message
    if(input.toString()){
      process.stdout.write('\033[2A');
      process.stdout.write('\033[2K');
      tempMsg = '';

      // Send json data to server
      client.write(JSON.stringify({'content': input}));
    }
  });

  // Print message when connection close
  client.on('close', () => {
    cs.red('\nConnection closed\n');

    // Destroy socket connection
    client.destroy();
    process.exit();
  });

  process.on('SIGINT', () => client.destroy());
  process.on('exit', () => client.destroy());
  process.on('uncaughtException', () => client.destroy());
}

// Function for add nickname and start chat connetion
function addUser(callback){
  readline.question('Enter you name for join to chat (minlen 3. Ctrl+C for exit)\n', name => {
    // If nickname valid set nickname and call callback function
    if(name && name.length >= 3){
      nickname = name;
      if(callback && typeof(callback) === 'function')
        callback();
      return true;
    }
    addUser(callback);
  });
}
