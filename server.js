// Modules
const net = require('net');
const Logger = require('./lib/logger');
const Message = require('./lib/message');

let activeSockets = [];

// Set information about all connections (connect/disconnect) to the log file
let connectionsLog = new Logger('./logs/connections.log');
// Set evety information about messages
let messagesLog = new Logger('./logs/messages.log');

// TCP Server creating
let server = net.createServer();

// Listening new socket connection
server.on('connection', socket => {

  // Handling recieved data
  socket.on('data', data => {
    let message = JSON.parse(data);
    handleMessage(message, socket);
  });

  socket.on('error', () => socket.emit('end'));
  socket.on('end', () => removeClient(socket));
});

server.listen(3000, '127.0.0.1', () => {
  renderCurrentServerState();
});


function removeClient(socket) {
  for (let pos in activeSockets){
    if (activeSockets[pos] === socket){
      activeSockets.splice(pos, 1);
    }
  }
  let message = new Message(socket.nickname, new Date(), `left the chat`);

  // Render server state
  leaveChat(message.nickname);

  // Sending message for all users that current client left chat
  broadcast(JSON.stringify(message), socket);
}

function handleMessage(messageRequest, socket){
  let message;

  // Creating new user if we get request with bogy(cmd = 'setName', content = 'Name')
  if(messageRequest.cmd === 'setName' && messageRequest.content){

    message = new Message(messageRequest.content, new Date(), '');
    socket.nickname = message.nickname;

    // Chech unique nickname (if false - send used info about error)
    if (isUserExist(message.nickname)) {
      message.content = 'user exist';
      socket.write(JSON.stringify(message));
    } else {
      // Message for all clients about new user
      message.content = 'joined the chat';
      // Saving connected socket
      activeSockets.push(socket);
      // Render server state
      joinChat(message.nickname);
      broadcast(JSON.stringify(message), socket);
    }
  } else {
    // Standart message treatment
    message = new Message(socket.nickname, new Date(), messageRequest.content);
    broadcast(JSON.stringify(message), socket);

    // Logging all messages
    messagesLog.log(message.nickname + ' PRINT : ' + message.content);
  }
}

/*
  Function userd for logging connections and rendering the current server state
*/
function joinChat(info) {
  renderCurrentServerState();
  connectionsLog.log('CONNECTED: ' + info);
}

/*
  Function userd for logging connections and rendering the current server state
*/
function leaveChat(info) {
  renderCurrentServerState();
  connectionsLog.log('DISCONNECTED: ' + info);
}

/*
  Console rendering
*/
function renderCurrentServerState() {
  process.stdout.write('\033[2J');
  process.stdout.cursorTo(0, 0);
  process.stdout.write('Amount of connected clients: ' + activeSockets.length);
}

/**
 * Sending messages for all sockets
 */
function broadcast(message, socket) {
  for (let client of activeSockets) {
    client.write(message);
  }
}

/**
 * Check name for setting name for new client. Name must be uniqie.
 */
function isUserExist(name) {
  for (let socket of activeSockets) {
    if (name === socket.nickname) {
      return true;
    }
  }
  return false;
}
