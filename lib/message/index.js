class Message {
  constructor(nickname, timestamp, content) {
      this.nickname = nickname;
      this.timestamp = timestamp;
      this.content = content;
  }
}

module.exports = Message;
