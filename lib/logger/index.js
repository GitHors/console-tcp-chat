const winston = require('winston');
winston.emitErrs = true;

class Logger {

  constructor(file) {
    this._logger = new winston.Logger({
        transports: [
            new winston.transports.File({
                filename: file,
                timestamp: true
            })
        ],
        exitOnError: false
    });
  }

  log(info) {
    this._logger.info(info);
  }
}

module.exports = Logger;
